package com.yaorange.dao;

import com.yaorange.entity.City;
import com.yaorange.entity.Province;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhuzhengyi
 * @date 2019/09/19
 */
@Mapper
public interface ProvinceMapper extends tk.mybatis.mapper.common.Mapper<Province> {
}
