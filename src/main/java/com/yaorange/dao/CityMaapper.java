package com.yaorange.dao;

import com.yaorange.entity.City;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhuzhengyi
 * @date 2019/09/19
 */
@Mapper
public interface CityMaapper extends tk.mybatis.mapper.common.Mapper<City> {
}
