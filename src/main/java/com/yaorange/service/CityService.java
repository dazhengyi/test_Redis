package com.yaorange.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yaorange.dao.CityMaapper;
import com.yaorange.entity.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author zhuzhengyi
 * @date 2019/09/19
 */
@Service
public class CityService {
    @Autowired
    private CityMaapper cityMaapper;
   /* @Autowired
    private JedisPool jedisPool;*/
    @Autowired
    private RedisTemplate redisTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    public String findByProvinceID(Integer id){
        /*Jedis jedis = jedisPool.getResource();
        String cityList = jedis.get("cityList");*/
        String cityList = (String)redisTemplate.opsForValue().get(""+id);
        if (cityList==null) {
            Example example = new Example(City.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("p_id",id);
            List<City> cities = cityMaapper.selectByExample(example);
            try {
                //List<City> --> json
                String string = objectMapper.writeValueAsString(cities);
                redisTemplate.opsForValue().set(""+id,string);
                //返回查询的数据
                return string;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }else {
            return cityList;
        }



        return null;
    }

}
