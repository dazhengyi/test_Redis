package com.yaorange.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yaorange.dao.ProvinceMapper;
import com.yaorange.entity.Province;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;

/**
 * @author zhuzhengyi
 * @date 2019/09/19
 */
@Service
public class ProvinceService {
    @Autowired
    private ProvinceMapper province;
   /* @Autowired
    private JedisPool jedisPool;*/
    @Autowired
    private RedisTemplate redisTemplate;

    ObjectMapper objectMapper = new ObjectMapper();
    public String findAllProvince(){
       /* Jedis jedis = jedisPool.getResource();
        String provinList = jedis.get("provinList");*/
        String provinList = (String)redisTemplate.opsForValue().get("provinList");
        if(null==provinList){
            List<Province> provinces = province.selectAll();

            try {
                //List<Province>   -->   json
                String string = objectMapper.writeValueAsString(provinces);
                redisTemplate.opsForValue().set("provinList",string);
                return string;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        } else {
            return provinList;
        }
        return  null;

    }
}
