package com.yaorange.web;

import com.yaorange.entity.City;
import com.yaorange.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhuzhengyi
 * @date 2019/09/19
 */
@RestController
@RequestMapping("/cityController")
public class CityController {
    @Autowired
    CityService cityService;
    @GetMapping
    public String findAll( Integer id){

        return cityService.findByProvinceID(id);
    }
}
