package com.yaorange.web;

import com.yaorange.entity.Province;
import com.yaorange.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhuzhengyi
 * @date 2019/09/19
 */
@RestController
@RequestMapping("/provinceController")
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;
    @GetMapping
    public String findAll(){
        return provinceService.findAllProvince();
    }
}
